﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;

namespace ClassTask5RosendoB
{
    /// <summary>
    /// Class to store sigma, max and min values of a kernel applied to a pixel.
    /// </summary>
    public class KernelDecisionValues
    {
        public Channel BChannel { get; }
        public Channel GChannel { get; }
        public Channel RChannel { get; }

        // Initialize each channel to default values 
        public KernelDecisionValues()
        {
            BChannel = new Channel()
            {
                MaxValue = 0,
                MinValue = 255,
                Sigma = 0
            };
            GChannel = new Channel()
            {
                MaxValue = 0,
                MinValue = 255,
                Sigma = 0
            };
            RChannel = new Channel()
            {
                MaxValue = 0,
                MinValue = 255,
                Sigma = 0
            };
        }

        /// <summary>
        /// Computes sigma, max and min values of a kernel applied to a pixel.
        /// </summary>
        /// <param name="kernelPixel">Current image pixel inside kernel</param>
        /// <param name="sumSigma">Avoid or not to sum center pixel in kernel to sigma</param>
        public void CalculateValues(Vec3b kernelPixel, bool sumSigma)
        {
            this.BChannel.Sigma += sumSigma ? kernelPixel.Item0 : 0;
            this.BChannel.MaxValue = Math.Max(kernelPixel.Item0, this.BChannel.MaxValue);
            this.BChannel.MinValue = Math.Min(kernelPixel.Item0, this.BChannel.MinValue);

            this.GChannel.Sigma += sumSigma ? kernelPixel.Item1 : 0;
            this.GChannel.MaxValue = Math.Max(kernelPixel.Item1, this.GChannel.MaxValue);
            this.GChannel.MinValue = Math.Min(kernelPixel.Item1, this.GChannel.MinValue);

            this.RChannel.Sigma += sumSigma ? kernelPixel.Item2 : 0;
            this.RChannel.MaxValue = Math.Max(kernelPixel.Item2, this.RChannel.MaxValue);
            this.RChannel.MinValue = Math.Min(kernelPixel.Item2, this.RChannel.MinValue);
        }
    }

    /// <summary>
    /// Contains sigma, max and min values of channel.
    /// </summary>
    public class Channel
    {
        /// <summary>
        /// Contains the sum of all pixel values inside a kernel except the center pixel.
        /// </summary>
        public float Sigma { get; set; }
        /// <summary>
        /// Contains the maximum pixel value found in the kernel.
        /// </summary>
        public float MaxValue { get; set; }
        /// <summary>
        /// Contains the minimum pixel value found in the kernel.
        /// </summary>
        public float MinValue { get; set; }
    }

}
