﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterfaceImageOperators;
using OpenCvSharp;

namespace ClassTask5RosendoB
{
    public class ImageOperationsRosendo : IImageOperators
    {
        #region Construct Indexers
        // Return an indexer used in each matrix traversal 
        private MatIndexer<Vec3b> GetVec3bIndexer(Mat img)
        {
            Mat<Vec3b> matVectOrig = new Mat<Vec3b>(img);
            return matVectOrig.GetIndexer();
        }

        private MatIndexer<float> GetFloatIndexer(Mat img)
        {
            Mat<float> matVectOrig = new Mat<float>(img);
            return matVectOrig.GetIndexer();
        }

        private MatIndexer<byte> GetByteIndexer(Mat img)
        {
            Mat<byte> matVectOrig = new Mat<byte>(img);
            return matVectOrig.GetIndexer();
        }
        #endregion

        #region Basic operations
        public Mat AddConstant(Mat img, float constantValue)
        {
            Mat[] channels;
            Cv2.Split(img, out channels);

            for (int i = 0; i < img.Channels(); i++)
                channels[i] += constantValue;

            Mat dstImage = new Mat();
            Cv2.Merge(channels, dstImage);

            return dstImage;
        }

        #region Clear image

        // Main Clear function used in overloading 
        private Mat Clear(Mat img, float value)
        {
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var matVectDst = new Mat<Vec3b>(dstImage); // cv::Mat_<cv::Vec3b>
            var indexerDst = matVectDst.GetIndexer();

            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    indexerDst[y, x] = new Vec3b((byte)value, (byte)value, (byte)value);
                }
            }

            return dstImage;
        }

        public Mat ClearImage(Mat img, float value)
        {
            return this.Clear(img, value);
        }

        public Mat ClearImage(Mat img)
        {
            return this.Clear(img, 0);
        }

        #endregion 

        public Mat CopyImage(Mat img)
        {
            var indexerOrig = this.GetVec3bIndexer(img);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
                for (int x = 0; x < img.Width; x++)
                    indexerDst[y, x] = indexerOrig[y, x];

            return dstImage;
        }

        public Mat InvertImage(Mat img)
        {
            var indexerOrig = this.GetVec3bIndexer(img);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    var pixel = indexerOrig[y, x];

                    // Loop over channels to get new value
                    for (int i = 0; i < img.Channels(); i++)
                        pixel[i] = (byte)(255 - pixel[i]);

                    indexerDst[y, x] = pixel;
                }
            }

            return dstImage;
        }

        #region Scaling
        // Function that returns a valid value for the Scale process
        private byte ValidScaleValue(float value)
        {
            if (value < 0) return 0;
            if (value > 255) return 255;

            return (byte)value;
        }
        public Mat Scaling(Mat img, float gamma, float beta)
        {
            var indexerOrig = this.GetVec3bIndexer(img);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    var pixel = indexerOrig[y, x];

                    // Loop over channels to get new value
                    for (int i = 0; i < img.Channels(); i++)
                        pixel[i] = ValidScaleValue(pixel[i] * gamma + beta);

                    indexerDst[y, x] = pixel;
                }
            }

            return dstImage;
        }
        #endregion

        #region Shift Image

        /// <summary>
        /// Returns a new shifted Point
        /// </summary>
        /// <param name="xPixel">X coordinate of the current pixel location</param>
        /// <param name="yPixel">X coordinate of the current pixel location</param>
        /// <param name="dx">Number of steps in X coordinate</param>
        /// <param name="dy">Number of steps in Y coordinate</param>
        /// <returns></returns>
        private Point GetNewShiftPoint(float xPixel, float yPixel, float dx, float dy)
        {
            return new Point(xPixel + dx, yPixel + dy);
        }

        /// <summary>
        /// Returns a new shifted Point with a direction
        /// </summary>
        /// <param name="direction">Translation orientation (top, right, left, right, top-right, top-left, bottom-right, bottom-left)</param>
        /// <param name="xPixel">X coordinate of the current pixel location</param>
        /// <param name="yPixel">X coordinate of the current pixel location</param>
        /// <param name="dx">Number of steps in X coordinate</param>
        /// <param name="dy">Number of steps in Y coordinate</param>
        /// <returns></returns>
        private Point GetNewShiftPoint(DirectionsEnum direction, float xPixel, float yPixel, float dx, float dy)
        {
            Point offsetPoint = new Point();

            switch (direction)
            {
                case DirectionsEnum.Top:
                    offsetPoint = this.GetNewShiftPoint(xPixel, yPixel, 0, -Math.Abs(dx));
                    break;
                case DirectionsEnum.Bottom:
                    offsetPoint = this.GetNewShiftPoint(xPixel, yPixel, 0, Math.Abs(dx));
                    break;
                case DirectionsEnum.Right:
                    offsetPoint = this.GetNewShiftPoint(xPixel, yPixel, Math.Abs(dx), 0);
                    break;
                case DirectionsEnum.Left:
                    offsetPoint = this.GetNewShiftPoint(xPixel, yPixel, -Math.Abs(dx), 0);
                    break;
                case DirectionsEnum.TopRight:
                    offsetPoint = this.GetNewShiftPoint(xPixel, yPixel, Math.Abs(dx), -Math.Abs(dy));
                    break;
                case DirectionsEnum.TopLeft:
                    offsetPoint = this.GetNewShiftPoint(xPixel, yPixel, -Math.Abs(dx), -Math.Abs(dy));
                    break;
                case DirectionsEnum.BottomRight:
                    offsetPoint = this.GetNewShiftPoint(xPixel, yPixel, Math.Abs(dx), Math.Abs(dy));
                    break;
                case DirectionsEnum.BottomLeft:
                    offsetPoint = this.GetNewShiftPoint(xPixel, yPixel, -Math.Abs(dx), Math.Abs(dy));
                    break;
                default:
                    break;
            }

            return offsetPoint;
        }

        private Mat Shift(Mat img, float dx, float dy, DirectionsEnum direction = 0, bool withDirection = false)
        {
            var indexerOrig = this.GetVec3bIndexer(img);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b origPixel = indexerOrig[y, x];
                    Point newLocation = withDirection ? this.GetNewShiftPoint(direction, x, y, dx, dy) : this.GetNewShiftPoint(x, y, dx, dy);
                    if ((newLocation.X < img.Width && newLocation.X >= 0) && (newLocation.Y < img.Height && newLocation.Y >= 0))
                        indexerDst[newLocation.Y, newLocation.X] = origPixel;
                }
            }

            return dstImage;
        }

        public Mat ShiftImage(Mat img, DirectionsEnum direction, float steps, float optionalSteps = 0)
        {
            return this.Shift(img, steps, optionalSteps, direction, true);
        }

        public Mat ShiftImage(Mat img, float xSteps, float ySteps)
        {
            return this.Shift(img, xSteps, ySteps);
        }

        public Mat ShiftImage(Mat img, Mat translationMatrix)
        {
            float xSteps = translationMatrix.At<float>(0, 2);
            float ySteps = translationMatrix.At<float>(1, 2);

            return this.Shift(img, xSteps, ySteps);
        }

        #endregion

        #endregion

        #region Kernel based operations
        public Mat BasicConvolutionImage(Mat img, Mat convolutionMask)
        {
            Mat imgFrame = new Mat();
            Cv2.CopyMakeBorder(img, imgFrame, 1, 1, 1, 1, BorderTypes.Default);

            var indexerOrig = this.GetVec3bIndexer(imgFrame);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            var indexerKernel = this.GetFloatIndexer(convolutionMask);

            for (int y = 1; y < imgFrame.Height - 1; y++)
            {
                for (int x = 1; x < imgFrame.Width - 1; x++)
                {
                    var pixel = indexerOrig[y, x];
                    int xKernel = 0;
                    int yKernel = 0;
                    float sigmaB = 0;
                    float sigmaG = 0;
                    float sigmaR = 0;

                    for (int k = y - 1; k <= y + 1; k++)
                    {
                        for (int n = x - 1; n <= x + 1; n++)
                        {
                            sigmaB += indexerOrig[k, n].Item0 * indexerKernel[yKernel, xKernel];
                            sigmaG += indexerOrig[k, n].Item1 * indexerKernel[yKernel, xKernel];
                            sigmaR += indexerOrig[k, n].Item2 * indexerKernel[yKernel, xKernel];

                            xKernel++;
                        }
                        xKernel = 0;
                        yKernel++;
                    }

                    pixel.Item0 = (byte)(sigmaB);
                    pixel.Item1 = (byte)(sigmaG);
                    pixel.Item2 = (byte)(sigmaR);

                    indexerDst[y - 1, x - 1] = pixel;
                }
            }

            return dstImage;
        }

        public Mat Dilation(Mat img)
        {
            Mat imgFrame = new Mat();
            Cv2.CopyMakeBorder(img, imgFrame, 1, 1, 1, 1, BorderTypes.Constant, Scalar.Black);

            var indexerOrig = this.GetVec3bIndexer(imgFrame);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 1; y < imgFrame.Height - 1; y++)
            {
                for (int x = 1; x < imgFrame.Width - 1; x++)
                {
                    var pixel = indexerOrig[y, x];
                    KernelDecisionValues info = new KernelDecisionValues();

                    for (int k = y - 1; k <= y + 1; k++)
                        for (int n = x - 1; n <= x + 1; n++)
                            info.CalculateValues(indexerOrig[k, n], indexerOrig[k, n] != pixel);

                    if (info.BChannel.Sigma > info.BChannel.MinValue)
                        pixel.Item0 = (byte)info.BChannel.MaxValue;
                    if (info.GChannel.Sigma > info.GChannel.MinValue)
                        pixel.Item1 = (byte)info.GChannel.MaxValue;
                    if (info.RChannel.Sigma > info.RChannel.MinValue)
                        pixel.Item2 = (byte)info.RChannel.MaxValue;

                    indexerDst[y - 1, x - 1] = pixel;
                }
            }

            return dstImage;
        }

        public Mat Erosion(Mat img)
        {
            Mat imgFrame = new Mat();
            Cv2.CopyMakeBorder(img, imgFrame, 1, 1, 1, 1, BorderTypes.Constant, Scalar.White);

            var indexerOrig = this.GetVec3bIndexer(imgFrame);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 1; y < imgFrame.Height - 1; y++)
            {
                for (int x = 1; x < imgFrame.Width - 1; x++)
                {
                    var pixel = indexerOrig[y, x];
                    KernelDecisionValues info = new KernelDecisionValues();

                    for (int k = y - 1; k <= y + 1; k++)
                        for (int n = x - 1; n <= x + 1; n++)
                            info.CalculateValues(indexerOrig[k, n], indexerOrig[k, n] != pixel);

                    if (info.BChannel.Sigma < 8 * info.BChannel.MaxValue)
                        pixel.Item0 = (byte)info.BChannel.MinValue;
                    if (info.GChannel.Sigma < 8 * info.GChannel.MaxValue)
                        pixel.Item1 = (byte)info.GChannel.MinValue;
                    if (info.RChannel.Sigma < 8 * info.RChannel.MaxValue)
                        pixel.Item2 = (byte)info.RChannel.MinValue;

                    indexerDst[y - 1, x - 1] = pixel;
                }
            }

            return dstImage;
        }

        #region Remove noise operations
        public Mat RemoveNoise(Mat img)
        {
            Mat imgFrame = new Mat();
            Cv2.CopyMakeBorder(img, imgFrame, 1, 1, 1, 1, BorderTypes.Default);

            var indexerOrig = this.GetVec3bIndexer(imgFrame);

            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 1; y < imgFrame.Height - 1; y++)
            {
                for (int x = 1; x < imgFrame.Width - 1; x++)
                {
                    var pixel = indexerOrig[y, x];
                    KernelDecisionValues info = new KernelDecisionValues();

                    for (int k = y - 1; k <= y + 1; k++)
                        for (int n = x - 1; n <= x + 1; n++)
                            info.CalculateValues(indexerOrig[k, n], indexerOrig[k, n] != pixel);

                    if (info.BChannel.Sigma == info.BChannel.MinValue)
                        pixel.Item0 = (byte)info.BChannel.MinValue;
                    else if (info.BChannel.Sigma == 8 * info.BChannel.MaxValue)
                        pixel.Item0 = (byte)info.BChannel.MaxValue;
                    if (info.GChannel.Sigma == info.GChannel.MinValue)
                        pixel.Item1 = (byte)info.GChannel.MinValue;
                    else if (info.GChannel.Sigma == 8 * info.GChannel.MaxValue)
                        pixel.Item1 = (byte)info.GChannel.MaxValue;
                    if (info.RChannel.Sigma == info.RChannel.MinValue)
                        pixel.Item2 = (byte)info.RChannel.MinValue;
                    else if (info.RChannel.Sigma == 8 * info.RChannel.MaxValue)
                        pixel.Item2 = (byte)info.RChannel.MaxValue;

                    indexerDst[y - 1, x - 1] = pixel;
                }
            }

            return dstImage;
        }

        public Mat RemoveNoiseSpurs(Mat img)
        {
            Mat imgFrame = new Mat();
            Cv2.CopyMakeBorder(img, imgFrame, 1, 1, 1, 1, BorderTypes.Constant, Scalar.White);

            var indexerOrig = this.GetVec3bIndexer(imgFrame);

            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 1; y < imgFrame.Height - 1; y++)
            {
                for (int x = 1; x < imgFrame.Width - 1; x++)
                {
                    var pixel = indexerOrig[y, x];
                    KernelDecisionValues info = new KernelDecisionValues();

                    for (int k = y - 1; k <= y + 1; k++)
                        for (int n = x - 1; n <= x + 1; n++)
                            info.CalculateValues(indexerOrig[k, n], indexerOrig[k, n] != pixel);

                    if (info.BChannel.Sigma < 2 * info.BChannel.MaxValue)
                        pixel.Item0 = (byte)info.BChannel.MinValue;
                    else if (info.BChannel.Sigma > 6 * info.BChannel.MaxValue)
                        pixel.Item0 = (byte)info.BChannel.MaxValue;

                    if (info.GChannel.Sigma < 2 * info.GChannel.MaxValue)
                        pixel.Item1 = (byte)info.GChannel.MinValue;
                    else if (info.GChannel.Sigma > 6 * info.GChannel.MaxValue)
                        pixel.Item1 = (byte)info.GChannel.MaxValue;

                    if (info.RChannel.Sigma < 2 * info.RChannel.MaxValue)
                        pixel.Item2 = (byte)info.RChannel.MinValue;
                    else if (info.RChannel.Sigma > 6 * info.RChannel.MaxValue)
                        pixel.Item2 = (byte)info.RChannel.MaxValue;

                    indexerDst[y - 1, x - 1] = pixel;
                }
            }

            return dstImage;
        }

        public Mat RemovePepperNoise(Mat img)
        {
            Mat imgFrame = new Mat();
            Cv2.CopyMakeBorder(img, imgFrame, 1, 1, 1, 1, BorderTypes.Default);

            var indexerOrig = this.GetVec3bIndexer(imgFrame);

            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 1; y < imgFrame.Height - 1; y++)
            {
                for (int x = 1; x < imgFrame.Width - 1; x++)
                {
                    var pixel = indexerOrig[y, x];
                    KernelDecisionValues info = new KernelDecisionValues();

                    for (int k = y - 1; k <= y + 1; k++)
                        for (int n = x - 1; n <= x + 1; n++)
                            info.CalculateValues(indexerOrig[k, n], indexerOrig[k, n] != pixel);

                    if (info.BChannel.Sigma == info.BChannel.MinValue)
                        pixel.Item0 = (byte)info.BChannel.MinValue;
                    if (info.GChannel.Sigma == info.GChannel.MinValue)
                        pixel.Item1 = (byte)info.GChannel.MinValue;
                    if (info.RChannel.Sigma == info.RChannel.MinValue)
                        pixel.Item2 = (byte)info.RChannel.MinValue;

                    indexerDst[y - 1, x - 1] = pixel;
                }
            }

            return dstImage;
        }

        public Mat RemoveSaltNoise(Mat img)
        {
            Mat imgFrame = new Mat();
            Cv2.CopyMakeBorder(img, imgFrame, 1, 1, 1, 1, BorderTypes.Constant, Scalar.Black);

            var indexerOrig = this.GetVec3bIndexer(imgFrame);

            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 1; y < imgFrame.Height - 1; y++)
            {
                for (int x = 1; x < imgFrame.Width - 1; x++)
                {
                    var pixel = indexerOrig[y, x];
                    KernelDecisionValues info = new KernelDecisionValues();

                    for (int k = y - 1; k <= y + 1; k++)
                        for (int n = x - 1; n <= x + 1; n++)
                            info.CalculateValues(indexerOrig[k, n], indexerOrig[k, n] != pixel);

                    if (info.BChannel.Sigma == 8 * info.BChannel.MaxValue)
                        pixel.Item0 = (byte)info.BChannel.MaxValue;
                    if (info.GChannel.Sigma == 8* info.GChannel.MaxValue)
                        pixel.Item1 = (byte)info.GChannel.MaxValue;
                    if (info.RChannel.Sigma == 8 * info.RChannel.MaxValue)
                        pixel.Item2 = (byte)info.RChannel.MaxValue;

                    indexerDst[y - 1, x - 1] = pixel;
                }
            }

            return dstImage;
        }

        #endregion

        #endregion 

        #region Thresholding operations
        public Mat ThresholdImage(Mat img, float threshold)
        {
            var indexerOrig = this.GetVec3bIndexer(img);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b origPixel = indexerOrig[y, x];

                    // Loop over channels to get new value
                    for (int i = 0; i < img.Channels(); i++)
                        origPixel[i] = origPixel[i] > threshold ? (byte)255 : (byte)0;

                    indexerDst[y, x] = origPixel;
                }
            }

            return dstImage;
        }


        public Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold)
        {
            var indexerOrig = this.GetVec3bIndexer(img);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b origPixel = indexerOrig[y, x];

                    // Loop over channels to get new value
                    for (int i = 0; i < img.Channels(); i++)
                        origPixel[i] = (origPixel[i] > lowerThreshold) && (origPixel[i] < upperThreshold) ? (byte)255 : (byte)0;

                    indexerDst[y, x] = origPixel;
                }
            }

            return dstImage;
        }

        public Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold, bool invertedThresh)
        {
            var indexerOrig = this.GetVec3bIndexer(img);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b origPixel = indexerOrig[y, x];

                    // Loop over channels to get new value
                    for (int i = 0; i < img.Channels(); i++)
                        if ((origPixel[i] > lowerThreshold) && (origPixel[i] < upperThreshold))
                        {
                            origPixel[i] = invertedThresh ? (byte)0 : (byte)255;
                        }
                        else
                        {
                            origPixel[i] = !invertedThresh ? (byte)0 : (byte)255;
                        }

                    indexerDst[y, x] = origPixel;
                }
            }

            return dstImage;
        }

        public Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold, bool invertedThresh, float keepVal)
        {
            var indexerOrig = this.GetVec3bIndexer(img);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetVec3bIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
            {
                for (int x = 0; x < img.Width; x++)
                {
                    Vec3b origPixel = indexerOrig[y, x];

                    // Loop over channels to get new values
                    for (int i = 0; i < img.Channels(); i++)
                        if ((origPixel[i] > lowerThreshold) && (origPixel[i] < upperThreshold))
                        {
                            origPixel[i] = invertedThresh ? (byte)0 : keepVal >= 0 ? (byte)keepVal : origPixel[i];
                        }
                        else
                        {
                            origPixel[i] = !invertedThresh ? (byte)0 : keepVal >= 0 ? (byte)keepVal : origPixel[i];
                        }

                    indexerDst[y, x] = origPixel;
                }
            }

            return dstImage;
        }
        #endregion

        #region Binary exclusive operations
        public Mat BinaryBoundary(Mat img)
        {
            if (img.Channels() > 1)
            {
                var def = new Mat(img.Height, img.Width, MatType.CV_8UC1);
                def.PutText("Binary images only", new Point(10, 50), HersheyFonts.HersheyComplex, 0.9, Scalar.White);
                return def;
            }

            Mat imgFrame = new Mat();
            Cv2.CopyMakeBorder(img, imgFrame, 1, 1, 1, 1, BorderTypes.Default);

            var indexerOrig = this.GetByteIndexer(imgFrame);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetByteIndexer(dstImage);

            for (int y = 1; y < imgFrame.Height - 1; y++)
            {
                for (int x = 1; x < imgFrame.Width - 1; x++)
                {
                    var pixel = indexerOrig[y, x];
                    var sigma = 0;

                    for (int k = y - 1; k <= y + 1; k++)
                        for (int n = x - 1; n <= x + 1; n++)
                        {
                            sigma += indexerOrig[k, n];
                        }
                    sigma -= pixel;

                    if (sigma == 8 * 255)
                        pixel = 0;

                    indexerDst[y - 1, x - 1] = pixel;
                }
            }

            return dstImage;
        }

        public Mat VisualiseBinary(Mat img)
        {
            if (img.Channels() > 1)
            {
                var def = new Mat(img.Height, img.Width, MatType.CV_8UC1);
                def.PutText("Binary images only", new Point(10,50), HersheyFonts.HersheyComplex, 0.9, Scalar.White);
                return def;
            }

            var indexerOrig = this.GetByteIndexer(img);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetByteIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
                for (int x = 0; x < img.Width; x++)
                    indexerDst[y, x] = (byte)(255 - indexerOrig[y, x]);            

            return dstImage;
        }

        public Mat DiffBinary(Mat img, Mat erosionImg)
        {
            if ((img.Channels() > 1 || erosionImg.Channels() > 1) || img.Size() != erosionImg.Size())
            {
                var def = new Mat(img.Height, img.Width, MatType.CV_8UC1);
                def.PutText("Binary same size images only", new Point(10, 50), HersheyFonts.HersheyComplex, 0.9, Scalar.White);
                return def;
            }

            var indexerOrig = this.GetByteIndexer(img);
            var indexerErosion = this.GetByteIndexer(erosionImg);
            var dstImage = new Mat(img.Height, img.Width, img.Type());
            var indexerDst = this.GetByteIndexer(dstImage);

            for (int y = 0; y < img.Height; y++)
                for (int x = 0; x < img.Width; x++)
                    indexerDst[y, x] = (byte)(indexerOrig[y, x] - indexerErosion[y, x]);

            return dstImage;
        }

        #endregion

    }

}
